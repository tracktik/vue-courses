Further Links:

  Official Docs - Getting Started: http://vuejs.org/guide/
  Official Docs - Template Syntax: http://vuejs.org/guide/syntax.html
  Official Docs - Events: http://vuejs.org/guide/events.html
  Official Docs - Computed Properties & Watchers: http://vuejs.org/guide/computed.html
  Official Docs - Class and Style Binding: http://vuejs.org/guide/class-and-style.html



Good starting point:
  https://alligator.io/vuejs/