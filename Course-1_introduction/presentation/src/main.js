import Vue from 'vue/dist/vue'
import Eagle from 'eagle.js'
import 'eagle.js/dist/eagle.css'
import 'animate.css'
import 'flexboxgrid'

import App from './pages/App.vue'

Vue.use(Eagle)

new Vue({
  el: '#app',
  components: { App },
  template: '<App />'
})

if (module.hot) {
  module.hot.accept()
}
