export const globalRegistrationCodeBlock = {
  langague: 'html',
  content: `
    Vue.component('component-a', { /* ... */ })
    Vue.component('component-b', { /* ... */ })
    Vue.component('component-c', { /* ... */ })
    
    new Vue({ el: '#app' })
    
    <div id="app">
      <component-a></component-a>
      <component-b></component-b>
      <component-c></component-c>
    </div>
  `
}


export const localRegistrationCodeBlock = {
  langague: 'html',
  content: `
    var ComponentA = { /* ... */ }
    var ComponentB = { /* ... */ }
    var ComponentC = { /* ... */ }
    
    new Vue({
      el: '#app'
      components: {
        ComponentA,
        ComponentB
      }
    })
  `
}
