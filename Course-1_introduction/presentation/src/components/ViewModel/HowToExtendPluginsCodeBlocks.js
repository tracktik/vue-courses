export const writingPluginCodeBlock = {
  langague: 'html',
  content: `
    MyPlugin.install = function (Vue, options) {
      // 1. add global method or property
      Vue.myGlobalMethod = function () {
        // something logic ...
      }
    
      // 2. add a global asset
      Vue.directive('my-directive', {
        bind (el, binding, vnode, oldVnode) {
          // something logic ...
        }
        ...
      })
    
      // 3. inject some component options
      Vue.mixin({
        created: function () {
          // something logic ...
        }
        ...
      })
    
      // 4. add an instance method
      Vue.prototype.$myMethod = function (methodOptions) {
        // something logic ...
      }
    }
  `
}


export const usingPluginCodeBlock = {
  langague: 'html',
  content: `
    // When using CommonJS via Browserify or Webpack
    var Vue = require('vue')
    var VueRouter = require('vue-router')
    
    // Don't forget to call this
    Vue.use(VueRouter)
  `
}
