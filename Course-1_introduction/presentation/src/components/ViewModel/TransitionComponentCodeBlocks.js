export const transitionVueFileCodeBlock = {
  langague: 'html',
  content: `
  <template>
    <transition name="customFate">
      <main v-if="main"></main>
    </transition>
  </template>

  <script>
  export default {
    data() {
      return {
        : null
      }
    },

    mounted() {
      alligatorDefinition = {}
    }
  }
  </script>

  <style>
  .customFate-enter-active, .customFate-leave-active {
    transition: transform 3s;
  }

  .customFate-enter, .customFate-leave-to {
    transform: translateX(-100%);
  }

  .customFate-enter-to, .customFate-leave {
    transform: translateX(0);
  }
  </style>
  `
}
