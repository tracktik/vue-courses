export const vOnDirectivesCodeBlock = {
  langague: 'html',
  content: `
    <div>{{message}}</div>  // Simple interpolation
    <div v-text="message"></div> // Display as a text
    <div v-html="message"></div> // Interprate as html
    <div v-once>{{message}}</div> // Evaluate one-time
  `
}
