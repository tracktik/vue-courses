export const simpleDirectivesCodeBlock = {
  langague: 'html',
  content: `
    <template>
      // Simple interpolation
      <div>{{message}}</div>  

      // prefer this form
      <div v-text="message"></div>

      // interprate as html
      <div v-html="message"></div>

      // evaluate one-time
      <div v-once>{{message}}</div>
    <template>
  `
}
