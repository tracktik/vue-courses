export const vBindDirectivesDataCodeBlock = {
  langague: 'html',
  content: `
    data () {
      return {
        dynamicId: uuid.random(),
        isButtonDisabled: true
      }
    }
  `
}

export const vBindDirectivesTemplateCodeBlock = {
  langague: 'html',
  content: `
    <template>
      // On attributes
      <div v-bind:id="dynamicId"></div>
      <button v-bind:disabled="isButtonDisabled"></button>

      // With shorthand
      <div :id="dynamicId"></div>
      
      // Using Javascript expression
      {{ number + 1 }}
      
      {{ ok ? 'YES' : 'NO' }}
      
      {{ message.split('').reverse().join('') }}
      
      // dynamic binding
      <div v-bind:id="'list-' + id"></div>
    <template>
  `
}
