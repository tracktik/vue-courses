export const inputDataCodeBlock = {
  langague: 'javascript',
  content: `
    data() {
      return {
        existentialQuestion: 'Am I truly an human ?'  
      }
    }
  `
}

export const inputTemplateCodeBlock = {
  langague: 'html',
  content: `
    <h2>My deepest, darkest question: {{existentialQuestion}}</h2>
    <input v-model="existentialQuestion"/>
  `
}

export const checkboxDataCodeBlock = {
  langague: 'javascript',
  content: `
    data () {
      return {
        statementIsTrue: true  
      }
    }  
  `
}

export const checkboxTemplateCodeBlock = {
  langague: 'html',
  content: `
  <p>You have decided this statement is {{statementIsTrue}}</p>
  <label>
    <input type="checkbox" v-model="statementIsTrue"/>
    Is this statement true?
  </label>
  `
}

export const multiCheckboxDataCodeBlock = {
  langague: 'javascript',
  content: `
  data () {
    return {
      namesThatRhyme: []  
    }
  }
  `
}

export const multiCheckboxTemplateCodeBlock = {
  langague: 'html',
  content: `
  <label>
    <input 
      type="checkbox" 
      value="Daniel" 
      v-model="namesThatRhyme"/>
    Daniel
  </label>
  <label>
    <input 
      type="checkbox" 
      value="Nathaniel" 
      v-model="namesThatRhyme"
    />
    Nathaniel
  </label>
  `
}

export const multiRadioDataCodeBlock = {
  langague: 'javascript',
  content: `
  data() {
    return {
      statementIsTrue: 'Yes'
    }
  }
  `
}

export const multiRadioTemplateCodeBlock = {
  langague: 'html',
  content: `
  <label>
    <input 
      type="radio" 
      value="Yes" 
      v-model="statementIsTrue"
    />
    Yes, it is true.
  </label>
  <label>
    <input 
      type="radio" 
      value="No" 
      v-model="statementIsTrue"
    />
    No, not at all.
  </label>
  `
}

export const shortHandForTemplateCodeBlock = {
  langague: 'html',
  content: `
  <input v-model="username" />
  <input :value="username" @input="username = $event.target.value" />
  `
}