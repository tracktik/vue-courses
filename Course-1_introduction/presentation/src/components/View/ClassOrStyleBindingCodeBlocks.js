export const dataCodeBlock = {
  langague: 'html',
  content: `
    data: {
      isActive: true,
      selectedColor: 'blue',
      baseStyles: {
        fontWeight:'800',
        color: 'red'
      },
      overrideStyles: {
        color:'blue'
      },
    }
  `
}

export const templateCodeBlock = {
  langague: 'html',
  content: `
    <div :class="{'is-active': isActive}"></div>
    
    <div :style="{color: selectedColor}"></div>
    
    <p :style="[baseStyles, overrideStyles]">
      baseStyles and overrideStyles
    </p>
  `
}
