export const vOnDirectivesDataCodeBlock = {
  langague: 'javascript',
  content: `
  methods: {
    incrementCounter () {
      this.count += 1
    }
  }
  `
}

export const vOnDirectivesTemplateCodeBlock = {
  langague: 'html',
  content: `
  <template>
    <button v-on:click="incrementCounter">Add</button>
    <button @click="incrementCounter">Add</button>      
  <template>
  `
}

export const vOnEventModifiersTemplateCodeBlock = {
  langague: 'javascript',
  content: `
    <a href="test" @click.prevent="addToCount">Add</a>  
  `
}

export const vOnKeyModifiersTemplateCodeBlock = {
  langague: 'html',
  content: `
    <input :keyup.13="addToCount" v-model="addValue">   
  `
}