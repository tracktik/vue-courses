export const overviewCodeBlocks = {
  overview: `
  {
    name: 'MyComponentName',
    props: {},
    data () {},
    watch: {},
    computed: {},
    methods: {}
  }
  `,
  name: `
  {
    name: 'TrackTikCourseHelloWorld'
  }
  `,
  
  props: `
  {
    props: {  // see that as arguments for function
      username: {
        type: [String, Object],
        default () {
          return 'anonymous'
        }
      },
    },
  `,
  
  data: `
  {
    data () {
      return {
        show: true,
        todos: {},
        $todoSelected: []
      }
    }
  }
  `,
  
  watch: `
  {
    data () {
      return {
        show: true,
        todos: {},
        $todoSelected: []
      }
    },

    watch: {
      todos (updatedTodos) {
        axios.put('/todos', updatedTodos)
      },
    }
  }
  `,
  
  computed: `
  {
    data () {
      return {
        show: true,
        todos: {},
        $todoSelected: []
      }
    },

    computed: {
      hasManyTodos () {
        return this.todos > 10
      },
  
      filterByStatus () {
        return (status) => {
          return this.todos.filter((todo) => todo.status === status)
        }
      }
    },
  }
  `,
  
  methods: `
  {
    data () {
      return {
        show: true,
        todos: {},
        $todoSelected: []
      }
    },

    methods: {
      addTodo (newTodo) {
        this.todos[] = newTodo
      }

      $doPrivateThing (secret) {
      }
    }
  }
  `
}

export const lifeCycleCodeBlock = `
{
  beforeCreate(){},
  created(){},
  beforeMount(){},
  mounted(){},
  beforeUpdate(){},
  updated(){},
  beforeDestroy(){},
  destroy(){}  
}`

export const fullPropsCodeBlock = `
{
  props: {
    lastName: {
      type: String
    },
    firstName: {
      type: String
    }
  },
  data () {
    return {
      show: false,
    }
  },
  computed: {
    fullName () {
      return this.firstName + this.lastName
    },
  },
  methods: {
    toogleShow () {
      this.show = !this.show
    }
  },
  mounted() {
    this.toggleShow()
  }
}`