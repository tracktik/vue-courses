export const conditionalsDataCodeBlock = {
  langague: 'html',
  content: `
  {
    data () {
      return {
        show: false
        isAuth: true    
      }
    }
  }
  `
}

export const conditionalsTemplateCodeBlock = {
  langague: 'html',
  content: `
  <div v-if="!show">this is hidden</div>
  <div v-else-if="isAuthenticated">Your are authenticated</div>
  <div v-else>Hello Mr Anonymous</div>
  <div v-show="show">This is show</div>
  `
}

export const loopsArrayDataCodeBlock = {
  langague: 'html',
  content: `
  {
    data () {
      return {
        shoppingItems: [
          {price: '100$', name: 'xbox'},
          {id:  '200$', name: 'playstation'}
          {id: '300$', name: 'n64'}
        ]
      }
    }
  }
  `
}

export const loopsArrayTemplateCodeBlock = {
  langague: 'html',
  content: `
  <ul>
    <li v-for="item in shoppingItems">
      {{ item.name }} - {{ item.price }}
    </li>
  </ul>
  `
}

export const loopsObjDataCodeBlock = {
  langague: 'js',
  content: `
  data () {
    return {
      objectItems: {
        key1: 'item1',
        key2: 'item 2',
        key3: 'item3'
      }
    }
  }
  `
}

export const loopsObjTemplateCodeBlock = {
  langague: 'html',
  content: `
  <ul>
    <li v-for="(item, key, index) in objectItems">
      {{ item }} - {{ key }} - {{ index }}
    </li>
  </ul>
  `
}


export const loopsRangeCodeBlock = {
  langague: 'html',
  content: `
  <ul>
    <li v-for="item in 15">{{ item }}</li>
  </ul>
  `
}

export const loopsKeyCodeBlock = {
  langague: 'html',
  content: `
  <ul>
    <li v-for="item in shoppingItems" :key="item.name">
      {{ item.name }} - {{ item.price }}
    </li>
  </ul>

  `
}
