
export const modelCodeBlock = {
  language: 'javascript',
  content: `
    const currentAppState = {
      username: 'me',
      todos: {
        1: {
          id: 1,
          task: 'Complete this presentation'
        }
      }
    }
  `
}

export const viewModelCodeBlock = {
  language: 'javascript',
  content: `
    const vm = new Vue({
      el: '#app'
      components: {
        CardItem
      }
      data () {
        return {
          currentAppState
        }
      }
    })
  `
}

export const viewCodeBlock = {
  language: 'html',
  content: `
    <template>
      <div>
        <CardItem 
          v-for="todo in todos" 
          :key="todo.id"
        >
          <h1 v-text="todo.task" />
        </CardItem>
      </div>
    </template>
  `
}