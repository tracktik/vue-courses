export const basicInstallOnPhpCodeBlock = {
  language: 'php',
  content: `
    <?php

    // Somewhere in your favorite framework
    $this->renderScript('https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js');
    
    ?>
  `
}

export const basicInstallOnHtmlCodeBlock = {
  language: 'html',
  content: `
  <!DOCTYPE html>
  <html lang="en">
    <meta>
      <meta charset="UTF-8">
      <title>Hello World in Vue.js</title>
    </meta>
    
    <head>
      <script 
        src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"
      />
    </head>

    <body>
    
      <div id="hello-world-app">
        <h1>{{msg}}</h1>
      </div>

      <script>
        new Vue({
          el: "#hello-world-app",
          data() {
            return {
              msg: "Hello World!"
            }
          }
        })
      </script>
  
    </body>
  </html>
  `
}
