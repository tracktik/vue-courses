
export const basicInstallOnCliCodeBlock = {
  language: 'bash',
  content: `
  npm install -g vue-cli
  vue init webpack my-super-application
  cd my-super-application
  npm install
  npm run dev
  npm run build
  `
}

export const installResultCodeBlock = {
  language: 'bash',
  content: `
    build/
    config/
    src/
        assets/
            logo.png
        components/
            Hello.vue
        App.vue
        main.js
    static/
    test/
    .babelrc
    .editorconfig
    .eslingignore
    .eslintrc.js
    .gitignore
    README.md
    index.html
    package.json
  `
}