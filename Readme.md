# Vue Courses 🏫

### About the courses

No matter at which Metric you look VueJS is the Shooting Star in the World of JavaScript Frameworks !

Frontend Frameworks are extremely popular because they give us this reactive, great User Experience we know from Mobile Apps - but now in the Browser!

You may know Backbone, Ember, Angular 2 and ReactJS, well, VueJS combines the Best of both Frameworks and makes building anything from small Widgets to big, Enterprise-Level Apps a Breeze and a whole lot of Fun! And if you don't know the mentioned Frameworks: That's fine, too, this Course does not expect any knowledge of any other Frontend Framework

### This Course will be created step by step !


I will make it iteratively to create a lot of feedback loop.

We'll start at the very Basics, what Vue.js is and how it works before we move on to more Complex and Advanced Topics but I'll be honest: It's too much to fit it all into one sentence:

  - Course 1 (Introduction)
      What is VueJS and why would you use it?
      The Basics (including the basic syntax, understanding Templates and much more!)
      Using Components (and what Components are to begin with)
      All about Directives, Filters and Mixins
    
  - Course 2 (not defined yet)
      How to create an awesome Single-Page-Application (SPA) with Routing
      How to improve State Management by using Vuex

And all the accompanied with many Exercises and multiple Course Projects - because it isn't just about seeing Code, it's about using and practicing it!